'''
Filename: train.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: python train.py
Notes: Defines the various models for the traffic4cast main challenge
'''
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from model import Traffic4castModel
from dataloader import Traffic4castDataset
from tensorflow.keras.optimizers import Adam
from clr_callback import CyclicLR

#---------------------------------------------------------------------------------------------------
# # This block is used for training on tensorflow cloud
# import datetime
# import tensorflow_cloud as tfc
# from utilities import download_data
# from utilities import download_data_multithread
#
# # Set Cloud GCP bucket name
# GCP_BUCKET = "traffic4cast_models"
# MODEL_PATH = "berlin_model"
#
# # tfc.COMMON_MACHINE_CONFIGS['V100_4X'],
# tfc.run(
#     requirements_txt="requirements.txt",
#     distribution_strategy="auto",
#     chief_config= tfc.MachineConfig(
#         cpu_cores=16,
#         memory=60,
#         accelerator_type=tfc.AcceleratorType.NVIDIA_TESLA_V100,
#         accelerator_count=2,
#     ),
#     docker_image_bucket_name=GCP_BUCKET,
# )
#
# tensorboard_path = os.path.join(
#     "gs://", GCP_BUCKET, "logs", datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
# )
#
# callbacks = [
#     tf.keras.callbacks.TensorBoard(log_dir=tensorboard_path, histogram_freq=1),
#     tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=3),
# ]
#
# #download_data_multithread()
# download_data()
#---------------------------------------------------------------------------------------------------

# Create a MirroredStrategy.
strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

# construct our convolutional autoencoder
print("[INFO] building model...")

# Open a strategy scope.
with strategy.scope():
    # Everything that creates variables should be under the strategy scope.
    # In general this is only model construction & `compile()`.
    # model = Traffic4castModel.build()
    model = Traffic4castModel.build_down_skip()
    model.compile(loss="mse", optimizer=Adam(1e-7))

training_ds = Traffic4castDataset(batch_size=4, sub_set='training', city='BERLIN', in_length=12,
                                  out_length=12, mode='non-overlapping')

validation_ds = Traffic4castDataset(batch_size=4, sub_set='validation', city='BERLIN', in_length=12,
                                    out_length=12, mode='non-overlapping')

# Initialize the cyclical learning rate callback
method = "triangular"
print("[INFO] using '{}' method".format(method))
clr = CyclicLR(mode=method, base_lr=1e-7, max_lr=0.002, step_size=(3*training_ds.__len__()))

H = model.fit(training_ds, validation_data=validation_ds, epochs=28, callbacks=[clr], verbose=1)


# Plot history: MSE
plt.plot(H.history['loss'], label='MSE (training data)')
plt.plot(H.history['val_loss'], label='MSE (validation data)')
plt.title('MSE Loss for Berlin')
plt.ylabel('MSE value')
plt.xlabel('No. epoch')
plt.legend(loc="upper left")
plt.savefig('berlin_loss.png')
# plt.show()

# Save model
model.save('berlin_convlstmae_down.hdf5')

# Plot the learning rate history
N = np.arange(0, len(clr.history["lr"]))
plt.figure()
plt.plot(N, clr.history["lr"])
plt.title("Cyclical Learning Rate (CLR)")
plt.xlabel("Training Iterations")
plt.ylabel("Learning Rate")
plt.savefig("clr_Berlin.png")

#---------------------------------------------------------------------------------------------------
# This block is for evaluating model, giving loss scores to more significant figures
# model = tf.keras.models.load_model('berlin_convlstmae.hdf5')
# valid_scores = model.evaluate(valid_data, verbose=1)
# print(valid_scores)
#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
# # This block is for saving models trained using tensorflow cloud in a google cloud bucket
# # Save, load and evaluate the model
# if tfc.remote():
#     SAVE_PATH = os.path.join("gs://", GCP_BUCKET, MODEL_PATH)
#     model.save(SAVE_PATH)
#     model = tf.keras.models.load_model(SAVE_PATH)
#---------------------------------------------------------------------------------------------------
