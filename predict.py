'''
Filename: predict.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: python predict.py
Notes: Run in 'traffic4cast' virtual environment
'''
import os
import pprint
import numpy as np
import tensorflow as tf
from utilities import postprocess
from utilities import load_mask
from utilities import save_h5_file
from utilities import load_h5_file

# BASE_PATH = '/Users/jaysantokhi/Traffic4cast/datasets/'
BASE_PATH = '/data/traffic4cast/'
CITY = 'BERLIN'

# SUBMISSION_PATH = '/Users/jaysantokhi/Traffic4cast/test_submissions/'
SUBMISSION_PATH = '/data/traffic4cast/submissions/convlstmae_down/'

# get list of test files for a city
paths = []
files = []
path = BASE_PATH + CITY + '/' + 'testing'

# r=root, d=directories, f=files
for r, d, f in os.walk(path):
    for file in f:
        if '.h5' in file:
            files.append(os.path.join(r, file))

paths += files
paths.sort()

# load pre-trained model
model = tf.keras.models.load_model('berlin_convlstmae_down.hdf5')

mask = load_mask('berlin_mask.npy')

total_predictions = 0

# loop through the files
for file in paths:
    # for every path, load the data
    x_test = load_h5_file(file)

    no_of_predictions = x_test.shape[0]

    total_predictions += no_of_predictions

    # remove 9th channel (currently not used in training)
    x_test = x_test[:, :, :, :, 0:8]

    # preprocessing
    x_test = x_test.astype(np.float32)
    x_test /= 255.

    # make predictions, shape of processed_preds (no_of_predictions, 12, 495, 436, 8)
    raw_preds = model.predict(x_test, batch_size=no_of_predictions, verbose=1)

    # post process
    processed_preds = postprocess(raw_preds)

    # select desired prediction time bins
    pred_idx = [0, 1, 2, 5, 8, 11]
    processed_preds = processed_preds[:, pred_idx, :, :, :]

    # multiply predictions by binary mask and cast to appropriate data type
    b_mask = np.repeat(mask[np.newaxis,...], no_of_predictions, axis=0)
    processed_preds = np.multiply(processed_preds, b_mask)
    processed_preds = processed_preds.astype(np.uint8)

    # save the predicted tensor to directories of predictions in the correct format
    submission_path = SUBMISSION_PATH + CITY + '/' + file[-18:]
    save_h5_file(submission_path, processed_preds)

print(total_predictions)
