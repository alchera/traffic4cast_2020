'''
Filename: generate_mask.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: python generate_mask.py
Notes: Run in 'traffic4cast' virtual environment
'''
import os
import pprint
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from utilities import load_h5_file

# BASE_PATH = '/Users/jaysantokhi/Traffic4cast/datasets/'
BASE_PATH = '/data/traffic4cast/'
CITY = 'BERLIN'

# get list of test files for a city
paths = []
files = []
path = BASE_PATH + CITY + '/' + 'training'

# r=root, d=directories, f=files
for r, d, f in os.walk(path):
    for file in f:
        if '.h5' in file:
            files.append(os.path.join(r, file))

paths += files
paths.sort()

mask = np.zeros(shape=(495, 436, 8))
count = 1

# loop through the files
for file in paths:
    print('File:', count, 'of', len(paths))
    # pprint.pprint(file)
    data = load_h5_file(file)
    data = data[:,:,:, 0:8]
    data = np.mean(data, axis=0)
    # print(data.shape)

    data[data > 0] = 1

    mask = np.logical_or(mask, data)
    mask = mask*1
    count += 1


mask_to_save = np.repeat(mask[np.newaxis,...], 6, axis=0)
print(mask_to_save.shape)
with open('berlin_mask.npy', 'wb') as f:
    np.save(f, mask_to_save)

# fig = plt.figure()
# mask = mask*255
# plt.imshow(data[:,:,0:3], alpha=0.85)
# plt.imshow(data[:,:,3:6], alpha=0.85)
# plt.imshow(data[:,:,5:8], alpha=0.85)
# plt.show()
