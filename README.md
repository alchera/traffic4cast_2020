# Temporal Autoencoder with U-Net Style Skip-Connections for Frame Prediction

This repository contains code submitted for IARAI's Traffic4Cast 2020 Challenge.

The following link provides details on data for the traffic4cast competition:
https://github.com/iarai/NeurIPS2020-traffic4cast

Given an input of 12 frames, the following model predicts the following 12 frames.

![](model_architecture_figures/final_architecture.png)

## Steps

### 1) Install relevant packaged with ``` pip install -r requirements.txt ```

### 2) Set the Base Path for the competition data in ``` dataloader.py ```
```python
BASE_PATH = '/data/traffic4cast/'
```
In the above case, ```traffic4cast``` contains the dataset directories: BERLIN, ISTANBUL and MOSCOW. 

### 3) Train a model for each city using ``` python train.py ```

Make sure all city related parameters are consistent. 

```python
training_ds = Traffic4castDataset(sub_set='training', city='BERLIN')
validation_ds = Traffic4castDataset(sub_set='validation', city='BERLIN')
plt.savefig('berlin_loss.png')
model.save('berlin_convlstmae_down.hdf5')
plt.savefig("clr_berlin.png")
```

### 4) Generate binary mask for each city using ``` python generate_mask.py ```

Store these masks in current working directory.

Make sure all city related parameters are consistent. 

```python
CITY = 'BERLIN'
open('berlin_mask.npy', 'wb')
```

### 5) Make predictions for each city using ``` python predict.py ```

Make sure that all city related parameters are consistent. 

```python
CITY = 'BERLIN'
model = tf.keras.models.load_model('berlin_convlstmae_down.hdf5')
mask = load_mask('berlin_mask.npy')
```

Give the Base Path to the data, as done in ``` dataloader.py ```.
```python
BASE_PATH = '/data/traffic4cast/'
```

Give the Submission Path. This is a directory that contains empty folders for each of the cities ```BERLIN, ISTANBUL``` and ```MOSCOW```

```python
SUBMISSION_PATH = '/data/traffic4cast/submissions/convlstmae_down/'
```

### 6) Do this for each of the cities: ```BERLIN, ISTANBUL``` and ```MOSCOW```

## Contact
Should you need help, do not hesitate to contact us: jay@alcheratechnologies.com



