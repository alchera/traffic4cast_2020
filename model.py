'''
Filename: model.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: N/A
Notes: Defines the various models for the traffic4cast main challenge
'''
import tensorflow as tf
from tensorflow.keras import Sequential

from tensorflow.keras.layers import Add
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv3D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Reshape
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import MaxPooling3D
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import Conv3DTranspose
from tensorflow.keras.layers import BatchNormalization

from tensorflow.keras.models import Model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.initializers import RandomNormal


class Traffic4castModel:

    def simple_model():
        inputShape = (12, 495, 436, 8)
        inputs = Input(shape=inputShape)
        x = inputs
        x = Conv3D(9, (5,5,5), activation='relu', padding="same", data_format='channels_last')(x)
        model = Model(inputs, x)
        print(model.summary())
        return model

    def build():
        inputShape = (12, 495, 436, 8)

        # define the input to the encoder
        inputs = Input(shape=inputShape)
        skip = inputs
        # print(inputs.shape)
        # x = inputs

        x = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same', return_sequences=True, return_state=False,
                       activation='tanh', recurrent_activation='hard_sigmoid',
                       kernel_initializer='glorot_uniform', unit_forget_bias=True,
                       data_format='channels_last')(inputs)
        # x = BatchNormalization()(x)
        x = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same', return_sequences=False, return_state=True,
                       activation='tanh', recurrent_activation='hard_sigmoid',
                       kernel_initializer='glorot_uniform', unit_forget_bias=True,
                       data_format='channels_last')(x)[-1]
        # x = BatchNormalization()(x)

        #flatten, repeat and reshape
        x = Flatten()(x)

        # build encoder
        encoder = Model(inputs, x)
        print(encoder.summary())

        # define the input to the decoder
        latentInputs = Input(shape=x.shape[1])
        x = RepeatVector(12)(latentInputs)
        x = Reshape((12, 495, 436, 8))(x)

        x = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same', return_sequences=True, return_state=False,
                             activation='tanh', recurrent_activation='hard_sigmoid',
                             kernel_initializer='glorot_uniform', data_format='channels_last')(x)
        # x = BatchNormalization()(x)

        x = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same', return_sequences=True, return_state=False,
                             activation='relu', recurrent_activation='hard_sigmoid',
                             kernel_initializer='glorot_uniform', data_format='channels_last')(x)
        # x = BatchNormalization()(x)
        # print(x.shape)
        # x = Add()([x, skip])
        # x = Conv3D(filters=8, kernel_size=(3, 3, 3), activation="sigmoid", padding="same")(x)

        decoder = Model(latentInputs, x)
        print(decoder.summary())

        conv_lstm_autoencoder = Model(inputs, decoder(encoder(inputs)))
        print(conv_lstm_autoencoder.summary())

        return conv_lstm_autoencoder

    def build_stacked():
        seq = Sequential(
            [
                Input(shape=(None, 495, 436, 8)),  # Variable-length sequence of 40x40x1 frames
                ConvLSTM2D(filters=8, kernel_size=(7, 7), padding="same", return_sequences=True),
                #BatchNormalization(),
                ConvLSTM2D(filters=8, kernel_size=(7, 7), padding="same", return_sequences=True),
                #BatchNormalization(),
                ConvLSTM2D(filters=12, kernel_size=(7, 7), padding="same", return_sequences=True),
                #BatchNormalization(),
                ConvLSTM2D(filters=8, kernel_size=(7, 7), padding="same", return_sequences=True),
                #BatchNormalization(),
                #Conv3D(filters=8, kernel_size=(12, 7, 7), activation="sigmoid", padding="same"),
            ]
        )
        print(seq.summary())
        return seq

    def build_skip():
        ''' Testing skip connections using either the Add() or Concatenate() layers '''
        inputShape = (12, 495, 436, 8)
        inputs = Input(shape=inputShape)

        #define encoder
        enc_conv_lstm_1 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(inputs)

        enc_conv_lstm_2 = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(enc_conv_lstm_1)

        enc_conv_lstm_3 = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same',
                             return_sequences=False, return_state=True)(enc_conv_lstm_2)[-1]

        #enc_conv_lstm_3 = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same',
        #                     return_sequences=True, return_state=False)(enc_conv_lstm_2)

        #flatten, repeat and reshape
        #enc_conv_lstm_3_mean = tf.math.reduce_mean(enc_conv_lstm_3, axis=1)
        #latentDim = Flatten()(enc_conv_lstm_3_mean)
        latentDim = Flatten()(enc_conv_lstm_3)
        x = RepeatVector(12)(latentDim)
        x = Reshape((12, 495, 436, 12))(x)

        # define decoder
        # skip1 = Add()([x, enc_conv_lstm_3])
        #skip1 = Concatenate()([x, enc_conv_lstm_3])

        dec_conv_lstm_1 = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(x)

        # skip2 = Add()([dec_conv_lstm_1, enc_conv_lstm_2])
        #skip2 = Concatenate()([dec_conv_lstm_1, enc_conv_lstm_2])

        dec_conv_lstm_2 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(dec_conv_lstm_1)

        # skip3 = Add()([dec_conv_lstm_2, enc_conv_lstm_1])
        #skip3 = Concatenate()([dec_conv_lstm_2, enc_conv_lstm_1])

        dec_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(dec_conv_lstm_2)

        #build and return model
        model = Model(inputs=inputs, outputs=dec_conv_lstm_3)
        print(model.summary())
        # plot_model(model, "model_skip_add.png", show_shapes=True)
        return model

    def build_down():
        ''' ConvLSTM AE with downsampling using MaxPool3D and Upscaling using Conv3DTranspose '''
        inputShape = (12, 495, 436, 8)
        inputs = Input(shape=inputShape)

        #define encoder
        enc_conv_lstm_1 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(inputs)

        pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_1)

        enc_conv_lstm_2 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(pool1)

        enc_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=False, return_state=True)(enc_conv_lstm_2)[-1]

        #flatten, repeat and reshape
        latentDim = Flatten()(enc_conv_lstm_3)
        x = RepeatVector(12)(latentDim)
        # x = Reshape((12, 495, 436, 8))(x)
        x = Reshape((12, 247, 218, 8))(x)

        # define decoder

        dec_conv_lstm_1 = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(x)

        upsample = Conv3DTranspose(filters=12, kernel_size=(1, 2, 2), strides=(1,2,2), output_padding=(0, 1, 0))(dec_conv_lstm_1)

        dec_conv_lstm_2 = ConvLSTM2D(filters=12, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(upsample)

        dec_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(dec_conv_lstm_2)

        #build and return model
        model = Model(inputs=inputs, outputs=dec_conv_lstm_3)
        print(model.summary())
        return model

    def build_down_skip():
        ''' ConvLSTM AE with downsampling and skip connections'''
        # Score: 0.00127527075484
        inputShape = (12, 495, 436, 8)
        numFilters = 12
        inputs = Input(shape=inputShape)

        #define encoder
        enc_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(inputs)

        pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_1)

        enc_conv_lstm_2 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(pool1)

        enc_conv_lstm_3 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=False, return_state=True)(enc_conv_lstm_2)[-1]

        #flatten, repeat and reshape
        latentDim = Flatten()(enc_conv_lstm_3)
        x = RepeatVector(12)(latentDim)
        # x = Reshape((12, 495, 436, 8))(x)
        x = Reshape((12, 247, 218, numFilters))(x)

        # define decoder

        skip1 = Add()([x, enc_conv_lstm_3])

        dec_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(skip1)

        skip2 = Add()([dec_conv_lstm_1, enc_conv_lstm_2])

        upsample = tf.keras.layers.Conv3DTranspose(filters=numFilters, kernel_size=(1, 2, 2),
                                                   strides=(1,2,2), output_padding=(0, 1, 0))(skip2)


        dec_conv_lstm_2 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(upsample)

        skip3 = Add()([dec_conv_lstm_2, enc_conv_lstm_1])

        dec_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(skip3)

        #build and return model
        model = Model(inputs=inputs, outputs=dec_conv_lstm_3)
        # print(model.summary())
        #plot_model(model, "model_down_skip.png", show_shapes=True)
        return model

    def build_down_skip_conv():
        ''' ConvLSTM AE with downsampling, skip connections and Conv3D at input for feature sampling'''
        inputShape = (12, 495, 436, 8)
        numFilters = 14
        inputs = Input(shape=inputShape)

        #define encoder
        conv1 = Conv3D(filters=numFilters, kernel_size=(5, 5, 5), padding='same', activation='relu')(inputs)

        enc_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(conv1)

        pool1 = MaxPooling3D((1, 2, 2), padding='valid')(enc_conv_lstm_1)

        enc_conv_lstm_2 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(pool1)

        enc_conv_lstm_3 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=False, return_state=True)(enc_conv_lstm_2)[-1]

        #flatten, repeat and reshape
        latentDim = Flatten()(enc_conv_lstm_3)
        x = RepeatVector(12)(latentDim)
        # x = Reshape((12, 495, 436, 8))(x)
        x = Reshape((12, 247, 218, numFilters))(x)

        # define decoder

        skip1 = Add()([x, enc_conv_lstm_3])

        dec_conv_lstm_1 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(skip1)

        skip2 = Add()([dec_conv_lstm_1, enc_conv_lstm_2])

        upsample = tf.keras.layers.Conv3DTranspose(filters=numFilters, kernel_size=(1, 2, 2),
                                                   strides=(1,2,2), output_padding=(0, 1, 0))(skip2)


        dec_conv_lstm_2 = ConvLSTM2D(filters=numFilters, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(upsample)

        skip3 = Add()([dec_conv_lstm_2, enc_conv_lstm_1])

        dec_conv_lstm_3 = ConvLSTM2D(filters=8, kernel_size=(7, 7), padding='same',
                            return_sequences=True, return_state=False)(skip3)

        # Not sure if this layer is going to work or if softmax is appropriate
        #conv2 = Conv3D(filters=8, kernel_size=(1, 1, 1), padding='valid', activation='sigmoid')(dec_conv_lstm_3)

        #build and return model
        model = Model(inputs=inputs, outputs=dec_conv_lstm_3)
        print(model.summary())
        # plot_model(model, "model_down_skip_conv.png", show_shapes=True)
        return model

if __name__ == "__main__":
    # model = Traffic4castModel.build()
    # model = Traffic4castModel.build_skip()
    # model = Traffic4castModel.build_down()
    model = Traffic4castModel.build_down_skip()
    # model = Traffic4castModel.build_down_skip_conv()
