'''
Filename: validate.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: python validate.py (within a python 3.7.4 environment)
Notes: Calculate MSE loss score on validation set, loading saved model weights
'''
import tensorflow as tf
from model import Traffic4castModel
from dataloader import Traffic4castDataset

# Path to data
PATH_TO_PARENT_DATA_FOLDER = '/data/traffic4cast/'

# Path to model weights (ensure file names are <city>_weights.hdf5)
PATH_TO_WEIGHTS = 'model_weights/'

# Build and compile model
berlin_model = Traffic4castModel.build_down_skip()
istanbul_model = Traffic4castModel.build_down_skip()
moscow_model = Traffic4castModel.build_down_skip()

# Show model architecture
print(berlin_model.summary())

# Compile models
berlin_model.compile(loss="mse")
istanbul_model.compile(loss="mse")
moscow_model.compile(loss="mse")

# load pre-trained model weights
berlin_model.load_weights(PATH_TO_WEIGHTS + 'berlin_weights.hdf5')
istanbul_model.load_weights(PATH_TO_WEIGHTS + 'istanbul_weights.hdf5')
moscow_model.load_weights(PATH_TO_WEIGHTS + 'moscow_weights.hdf5')


# Data Generators for each city's validation set
validation_berlin = Traffic4castDataset(batch_size=4, sub_set='validation', city='BERLIN', in_length=12,
                                    out_length=12, mode='non-overlapping', path=PATH_TO_PARENT_DATA_FOLDER)

validation_istanbul = Traffic4castDataset(batch_size=4, sub_set='validation', city='ISTANBUL', in_length=12,
                                    out_length=12, mode='non-overlapping', path=PATH_TO_PARENT_DATA_FOLDER)

validation_moscow = Traffic4castDataset(batch_size=4, sub_set='validation', city='MOSCOW', in_length=12,
                                    out_length=12, mode='non-overlapping', path=PATH_TO_PARENT_DATA_FOLDER)


# This block is for evaluating model, giving loss scores to more significant figures
MSE_berlin = berlin_model.evaluate(validation_berlin, verbose=1)
MSE_istanbul = istanbul_model.evaluate(validation_istanbul, verbose=1)
MSE_moscow = moscow_model.evaluate(validation_moscow, verbose=1)

print('MSE for Berlin: ', MSE_berlin)
print('MSE for Istanbul: ', MSE_istanbul)
print('MSE for Moscow: ', MSE_moscow)
