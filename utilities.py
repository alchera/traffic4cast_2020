'''
Filename: utilities.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: N/A
Notes: Provides various utility function
'''
import os
import csv
import sys
import h5py
import time
import pprint
import numpy as np
import tensorflow as tf
# from pathlib import Path
# from google.cloud import storage

def load_h5_file(file_path):
    ''' Return tensor housed in h5 file given by specified file path '''
    # Load
    fr = h5py.File(file_path, 'r')
    a_group_key = list(fr.keys())[0]
    data = list(fr[a_group_key])

    # Transform to appropriate numpy array
    data=data[0:]
    data = np.stack(data, axis=0)
    fr.close()
    return data


def save_h5_file(file_path, data):
    ''' Save a given tensor as a h5 file at a desired location '''
    f = h5py.File(file_path, 'w')
    dset = f.create_dataset('array', shape=(data.shape), data=data, compression="gzip")
    f.close()
    return


def postprocess(data):
    ''' Rescale data to 0-255, round it and cast it to type uint8 '''
    # Clip outputs between 0, 1
    data = np.clip(data, 0, 1)

    # Rescale to 0-255
    data *= 255

    # Make all number integers
    data = np.around(data)
    data = data.astype(np.uint8)
    return data


def load_mask(file):
    ''' Load binary mask (.npy) as a numpy array '''
    with open(file, 'rb') as f:
        mask = np.load(f)
    return mask

# def download_data():
#     ''' Download training and validation machine used by GCP instance '''
#     client = storage.Client()
#     bucket = client.get_bucket('traffic4cast_dataset')
#     blobs = list(client.list_blobs(bucket))
#     count = 0
#     for blob in blobs:
#         if count % 10 == 0:
#             print(count)
#             print(blob.name)
#         if blob.name.endswith("/"):
#             continue
#         file_split = blob.name.split("/")
#         directory = "/".join(file_split[0:-1])
#         Path(directory).mkdir(parents=True, exist_ok=True)
#         blob.download_to_filename(blob.name)
#         count += 1
#     return

if __name__ == "__main__":

    start_time = time.time()
    data = load_h5_file('/Users/jaysantokhi/Traffic4cast/datasets/MOSCOW/training/2019-01-01_moscow_9ch.h5')
    # data = load_h5_file('/Users/jaysantokhi/Traffic4cast/datasets/BERLIN/testing/2019-07-02_test.h5')
    end_time = time.time()
    print('Time to load file', end_time - start_time, 'seconds.')
