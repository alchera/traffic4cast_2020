'''
Filename: dataloader.py
Author: Jay Santokhi (jay@alcheratechnologies.com)
Usage: N/A
Notes: Provides class for loading training and validation sets
'''
import os
import h5py
import time
import pprint
import random
import numpy as np
from tensorflow.keras.utils import Sequence


# BASE_PATH = '/Users/jaysantokhi/Traffic4cast/datasets/'
BASE_PATH = '/data/traffic4cast/'
# BASE_PATH = ''

class Traffic4castDataset(Sequence):
    def __init__(self, batch_size, city='BERLIN', sub_set='training', in_length=12, out_length=12, mode='non-overlapping', path=BASE_PATH):
        self.batch_size = batch_size
        self.city = city
        self.sub_set = sub_set
        self.slots_in_day = 288
        self.input_length = in_length
        self.output_length = out_length
        self.sequence_length = self.input_length + self.output_length
        self.path = path

        self.data_path = self._get_data_path()
        self.mode = mode
        # print(self.data_path)

        # Create time slots for each day
        self.paths_wt_time_slots = [[path, i] for path in self.data_path for i in self._split_slots()]
        # pprint.pprint(self.paths_wt_time_slots)

        # Shuffle data to aid robustness in training
        self.on_epoch_end()
        return

    def __len__(self):
        ''' Number of batch in the Sequences '''
        length = int(np.ceil(len(self.paths_wt_time_slots) / float(self.batch_size)))
        return length

    def __getitem__(self, index):
        # print("getting item")
        ''' Gets batch at position index '''
        # Get batch IDs for given index
        batch = self.paths_wt_time_slots[index * self.batch_size : (index + 1) * self.batch_size]

        # Load each sequence in the batch
        data = np.array([self._get_frames_seq(path, i, self.sequence_length) for path, i in batch])

        # Preprocessing
        data = data.astype(np.float32)
        data /= 255.

        # data = np.transpose(data, (0, 1, 4, 2, 3))
        x_batch = data[:, :-self.output_length]
        y_batch = data[:, -self.output_length:]

        if (self.input_length and self.output_length) == 1:
            x_batch = x_batch[:, 0, :, :, :]
            y_batch = y_batch[:, 0, :, :, :]

        # Remove the traffic incidents channel (9th channel)
        x_batch = x_batch[:,:,:,:,0:8]
        y_batch = y_batch[:,:,:,:,0:8]

        return x_batch, y_batch


    def on_epoch_end(self):
        ''' Method called at end of every epoch '''
        random.shuffle(self.paths_wt_time_slots)
        return

    def _get_frames_seq(self, path, idx, length):
        ''' Open and read h5 file from given path '''
        f = h5py.File(path, 'r')
        key = list(f.keys())[0]
        data = f[key][idx:idx+length]
        return data

    def _split_slots(self):
        ''' Split data into either non-overlapping or overlapping sequences '''
        if self.mode == 'non-overlapping':
            slots = list(range(0, self.slots_in_day - self.sequence_length + 1, self.sequence_length))

        if self.mode == 'overlapping':
            # all possible slots
            slots = list(range(self.slots_in_day - self.sequence_length+1))

            # overlapping by half sequnce length (6 in the default case)
            # slots = list(range(0, self.slots_in_day - self.sequence_length + 1, int(self.sequence_length/6)))
            # print(slots)

        return slots

    def _get_data_path(self):
        ''' Get all files for specified city '''
        paths = []
        files = []
        path = self.path + self.city + '/' + self.sub_set

        # r=root, d=directories, f=files
        for r, d, f in os.walk(path):
            for file in f:
                if '.h5' in file:
                    files.append(os.path.join(r, file))

        paths += files
        return paths



if __name__ == "__main__":
    start_time = time.time()
    train_data = Traffic4castDataset(batch_size=1, sub_set='training', city='BERLIN', in_length=12, out_length=12, mode='non-overlapping')
    x, y = train_data.__getitem__(index=0)
    print(x.shape)
    print(y.shape)
    print(train_data.__len__())

    # pred_inx = [0, 1, 2, 5, 8, 11]
    # x_pred = x[:, pred_inx, :, :, :]
    # print(x_pred.shape)

    # test_data = Traffic4castDataset(batch_size=1, sub_set='validation', city='BERLIN')
    # x, y = test_data.__getitem__(index=0)
    # print(x.shape)
    # print(test_data.__len__())

    end_time = time.time()
    print('Time to get data', end_time - start_time, 'seconds.')
